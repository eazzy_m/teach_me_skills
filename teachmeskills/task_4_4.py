original_list = [1,2,3,4,5]
new_list = []
while original_list:
    if len(original_list) > 1:
        new_list.append(original_list.pop(1))
    else:
        new_list.append(original_list.pop(0))

print(new_list)

original_list = [1,2,3,4,5]
new_list = []

for element in original_list:
    new_list.append(element)
new_list.append(new_list.pop(0))
print(new_list)
