def inch_in_sent(num):
    outcome = num * 2.54
    return (f'{num} inches is {outcome} centimetrs')

def sent_in_inch(num):
    outcome = num * 0.3937
    return (f'{num} centimetrs is {outcome} inches')

def kilometrs_in_miles(num):
    outcome = num * 0.621371
    return (f'{num} kilometrs is {outcome} miles')

def miles_in_kilometrs(num):
    outcome = num * 1.60934
    return (f'{num} miles is {outcome} kilometrs')

def funts_in_kilograms(num):
    outcome = num * 0.453592
    return (f'{num} funts is {outcome} kilograms')

def kilograms_in_funts(num):
    outcome = num * 2.20462
    return (f'{num} kilograms is {outcome} funts')

def ounce_in_grams(num):
    outcome = num * 28.3495
    return (f'{num} ounce is {outcome} grams')

def grams_in_ounce(num):
    outcome = num *0.035274
    return (f'{num} grams is {outcome} ounce')

def gallons_in_litres(num):
    # ожидается перевод импесркого галлона
    outcome = num * 4.54609
    return (f'{num} gallons is {outcome} litres')

def litres_in_gallons(num):
    # ожидается перевод в имперский галлон
    outcome = num * 0.219969
    return (f'{num} litres is {outcome} gallons')

def pints_in_litres(num):
    # ожидается перевод импесркой пинты
    outcome = num * 0.568261
    return (f'{num} pints is {outcome} litres')

def litres_in_pints(num):
    # ожидается перевод в имперскую пинту
    outcome = num * 1.75975
    return (f'{num} litres is {outcome} pints')
